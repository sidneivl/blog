<?php
/**
 * The main template file
 *
 * @link http://innovation.com
 *
 * @package WordPress
 * @subpackage Innovation
 * @since Innovation 1.0
 */

get_header(); 
?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		About
	</main>
</div>
<?php get_footer(); ?>