<?php
/**
 * The main template file
 *
 * @link http://innovation.com
 *
 * @package WordPress
 * @subpackage Innovation
 * @since Innovation 1.0
 */

get_header(); 
?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php if ( has_post_thumbnail() ) { ?>
			<div class="parallax-window post_image_internal" data-parallax="scroll" data-image-src="<?php the_post_thumbnail_url('full'); ?>">		        
				<div class="title">
					<h2>
						<?php the_title(); ?>
					</h2>
					<div class="line"></div>
				</div>
			</div>
		<?php } else { ?>
			<div class="post_internal">
				<div class="title">
					<h2>
						<?php the_title(); ?>
					</h2>
					<div class="line"></div>
				</div>
			</div>
		<?php } ?>
		<div class="main-container">
			<div class="post-container">					
				<?php
				// Start the loop.
				while ( have_posts() ) : the_post();

					// Include the single post content template.
					get_template_part( 'template-parts/content', 'internal' );

				endwhile;
				?>
				<div class="share">				
					<?php dynamic_sidebar( 'sharepost' ); ?>
				</div>
				<div class="nav-posts">
					<div class="left-post">
						<?php previous_post_link('< %link', '%title'); ?>
					</div>
					<div class="right-post">					
						<?php next_post_link('%link >', '%title'); ?>
					</div>
				</div>
			</div>
			<?php get_sidebar(); ?>
			
			<div class="ad_post">
				<div class="ad_index">banner</div>
			</div>
			<div class="comments">
				<?php echo do_shortcode('[fbcomments]'); ?>
			</div>
		</div>
	</main>
</div>
<script type="text/javascript">
	$('.parallax-window').parallax();
</script>
<?php get_footer(); ?>