<?php
/**
 * The main template file
 *
 * @link http://innovation.com
 *
 * @package WordPress
 * @subpackage Innovation
 * @since Innovation 1.0
 */

get_header(); 
?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="slider">
			<?php layerslider(1) ?>
		</div>
		<div class="ad_index">banner</div>
		<div class="main-container">
			<div class="left_posts">
				<div class="masonry">
					<?php if ( have_posts() ) :
							while ( have_posts() ) : the_post();
								get_template_part( 'template-parts/content', 'post');
							endwhile;
						else :
							get_template_part( 'template-parts/content', 'none' );
						endif;
					?>
				</div>
			<?php the_posts_pagination( array( 'mid_size' => 5, 'screen_reader_text' => ' ' ) ); ?>	
			</div>	
			<?php get_sidebar(); ?>
		</div>
	</main>
</div>
<?php get_footer(); ?>