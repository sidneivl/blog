<?php
/**
 * Functions and definitions
 *
 * @package WordPress
 * @subpackage Innovation
 * @since Innovation 1.0
 */

function register_my_menus() {
  register_nav_menus(
    array(
      'social' => __( 'social' ),
      'main' => __( 'main' )
     )
   );
 }
 add_action( 'init', 'register_my_menus' );

 add_theme_support( 'post-thumbnails' );

function themename_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Rightbar', 'theme_name' ),
        'id'            => 'rightbar'
    ) );

    register_sidebar( array(
        'name'          => __( 'Searchbar', 'theme_name' ),
        'id'            => 'searchbar'
    ) );

    register_sidebar( array(
        'name'          => __( 'Footercontent', 'theme_name' ),
        'id'            => 'footercontent'
    ) );

    register_sidebar( array(
        'name'          => __( 'Share post', 'theme_name' ),
        'id'            => 'sharepost'
    ) );
}
add_action( 'widgets_init', 'themename_widgets_init' );
add_filter( 'widget_text', 'shortcode_unautop' );
add_filter( 'widget_text', 'do_shortcode' );

function my_connection_types() {
    p2p_register_connection_type( array(
        'name' => 'posts_to_posts',
        'from' => 'post',
        'to' => 'post'
    ) );
}
add_action( 'p2p_init', 'my_connection_types' );

function wmpudev_enqueue_icon_stylesheet() {
  wp_register_style( 'fontawesome', 'http:////maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' );
  wp_enqueue_style( 'fontawesome');
}
add_action( 'wp_enqueue_scripts', 'wmpudev_enqueue_icon_stylesheet' );

add_theme_support('html5', array('search-form'));
?>