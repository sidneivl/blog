<?php
/**
 * The template for displaying the footer
 *
 * @package WordPress
 * @subpackage Innovation
 * @since Innovation 1.0
 */
?>
<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="space"></div>
	<div class="content">
		<div class="inner">
			<?php dynamic_sidebar( 'footercontent' ); ?>
		</div>
	</div>
	<div class="menu-footer">
		<?php if(has_nav_menu( 'main' )) :
			wp_nav_menu( array(
				'theme_location' => 'main',
				'menu_class'     => 'footer-menu',
			 ) );							
		endif ?>		
	</div>
</footer>
<script src="<?php bloginfo('template_url'); ?>/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript">
	$('.masonry').masonry({
	  // options
	  itemSelector: '.item',
	  columnWidth: 302,
	  gutter: 20
	});

</script>
<?php wp_footer(); ?>
</body>
</html>