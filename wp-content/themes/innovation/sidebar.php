<?php
/**
 * The main template file
 *
 * @link http://innovation.com
 *
 * @package WordPress
 * @subpackage Innovation
 * @since Innovation 1.0
 */?>

	<aside id="secondary" class="sidebar widget-area" role="complementary">
		<!-- posts relacionados -->
		<?php
			// Find connected pages
			$connected = new WP_Query( array(
			  'connected_type' => 'posts_to_posts',
			  'connected_items' => get_queried_object(),
			  'nopaging' => true,
			) );

			// Display connected pages
			if ( $connected->have_posts() ) : ?>

			<div class="widget-side">
				<h2 class="widgettitle">Postes relacionados</h2>
				<?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
					<div class="post">
						<div class="post_image">
							<?php if ( has_post_thumbnail() ) : ?>
	        					<?php the_post_thumbnail(array( 75, 75 )); ?>
							<?php endif; ?>
						</div>
						<div class="post_content">
							<span class="title"><?php the_title(sprintf( '<h5 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h5>'); ?></span>
							<span class="date"><?php the_date(); ?></span>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		<?php 
			wp_reset_postdata();
			endif;
		?>

		<!-- categories -->
		<div class="widget-side">
			<h2 class="widgettitle">Categorias</h2>
			<?php
				$cats = wp_list_categories( array(
				                        'format' => 'array',
				                        'order' => 'ASC',
				                        'style' => 'none',
				                        'separator' => '',
				                        'echo' => 0
				                        )
				                     );

				if ($cats) {
				   echo '<ul class="wp-cat-cloud">';
				   echo $cats;
				   echo '</ul>';
				}
			?>
		</div>

		<!-- tags -->
		<div class="widget-side">
			<h2 class="widgettitle">Tags</h2>
			<?php
				$number_to_show = 10;
				$tags = wp_tag_cloud( array(
				                        'taxonomy' => 'post_tag',
				                        'format' => 'array',
				                        'order' => 'RAND',
				                        'number' => 0,
				                        'smallest' => '10',
				                        'largest' => '10',
				                        'echo' => 0,
				                        )
				                     );

				if ($tags) {
				   $max = ($number_to_show < sizeof($tags)) ? $number_to_show : sizeof($tags);
				   echo '<ul class="wp-tag-cloud">';
				   for ($i=0; $i < $max ; ++$i ) {
				      echo "<li>{$tags[$i]}</li>";
				   }
				   echo '</ul>';
				}
			?>
		</div>

		<!-- banners -->
		<div class="ad_sidebar">
			Banner
		</div>

		<!-- posts recentes -->
		<?php
			$recent_posts = new WP_Query( array( 'posts_per_page' => '5' ) );
			if ( $recent_posts->have_posts() ) :
		?>
		<div class="widget-side">
			<h2 class="widgettitle">Postes recentes</h2>
			<?php while ( $recent_posts->have_posts() ) : $recent_posts->the_post(); ?>
				<div class="post">
					<div class="post_image">
						<?php if ( has_post_thumbnail() ) : ?>
        					<?php the_post_thumbnail(array( 75, 75 )); ?>
						<?php endif; ?>
					</div>
					<div class="post_content">
						<span class="title"><?php the_title(sprintf( '<h5 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h5>'); ?></span>
						<span class="date"><?php the_date(); ?></span>
					</div>
				</div>
			<?php endwhile; ?>
		</div>
		<?php
			wp_reset_postdata();
			endif;
		?>

		<?php dynamic_sidebar( 'rightbar' ); ?>
	</aside>