<?php
/**
 * The tamplate part for post in home
 *
 *
 * @package WordPress
 * @subpackage Innovation
 * @since Innovation 1.0
 */
?>
<section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="content">
		<span class="date">				
			<span class='date_day'><?php the_time('j') ?></span><span class='date_month'><?php the_time('M') ?></span><span class='date_year'><?php the_time('y') ?></span>
		</span>

		<div class="text">
			<?php the_content(); ?>
		</div>

		<div class="line"></div>
	</div>
</section>