<?php
/**
 * The tamplate part for post in home
 *
 *
 * @package WordPress
 * @subpackage Innovation
 * @since Innovation 1.0
 */
?>
<div class="item">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( has_post_thumbnail() ) : ?>
	    	<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
	        	<?php the_post_thumbnail('medium'); ?>
	    	</a>
		<?php endif; ?>
		<div class="content">
			<?php the_title(sprintf( '<h4 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>'); ?>
			<span class="text">
				<?php  if (has_excerpt() ) {
					the_excerpt();
				}else{
					the_content('');
				} ?>
			</span>
			<span class="date">		
				<?php the_time( 'j M y' ); ?>
			</span>
		</div>
	</article>
</div>