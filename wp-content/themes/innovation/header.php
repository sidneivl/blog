<?php
/**
 * The template for displaying the header
 *
 * @package WordPress
 * @subpackage Innovation
 * @since Innovation 1.0
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css" type="text/css" media="all">
	<script src="<?php bloginfo('template_url'); ?>/js/jquery-3.1.0.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/parallax.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/masonry.pkgd.min.js"></script>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<nav class="nav-top">
	<div class="nav-content">
		<div class="nav-social">
			<ul>
				<li class="division"></li>
				<li>
					<a href="http://facebook.com"><i class="fa fa-facebook social"></i></a>
				</li>
				<li>
					<a href="http://twitter.com"><i class="fa fa-twitter social"></i></a>
				</li>
				<li>
					<a href="http://instagram.com"><i class="fa fa-instagram social"></i></a>
				</li>
				<li class="division"></li>
			</ul>
		</div>
	</div>
</nav>
<nav class="nav-main">
	<div class="nav-content">
		<div class="logo">
			<a href="/"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" /></a>
		</div>
		<div class="search">			
			<?php dynamic_sidebar( 'searchbar' ); ?>
		</div>
		<div class="menu">
			<?php if(has_nav_menu( 'main' )) :
				wp_nav_menu( array(
					'theme_location' => 'main',
					'menu_class'     => 'main-menu',
				 ) );							
			endif ?>
		</div>
	</div>
</nav>