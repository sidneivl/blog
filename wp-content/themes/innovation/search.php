<?php
/**
 * The template for displaying the header
 *
 * @package WordPress
 * @subpackage Innovation
 * @since Innovation 1.0
 */

get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>
			<div class="main-container">
				<header class="page-header">
					<h1 class="page-title"><?php printf( __( 'Encontramos para a busca: %s', 'twentysixteen' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?></h1>
				</header><!-- .page-header -->
				<div class="left_posts">
					<div class="masonry">

						<?php
						// Start the loop.
						while ( have_posts() ) : the_post();
							/**
							 * Run the loop for the search to output the results.
							 * If you want to overload this in a child theme then include a file
							 * called content-search.php and that will be used instead.
							 */
							get_template_part( 'template-parts/content', 'post' );

						// End the loop.
						endwhile;
					?>
					</div>	
				<?php the_posts_pagination( array( 'mid_size' => 5, 'screen_reader_text' => ' ' ) ); ?>						
				</div>
				<?php get_sidebar(); ?>
			</div>
		<?php
			// If no content, include the "No posts found" template.
			else :
				get_template_part( 'template-parts/content', 'none' );
			endif;
		?>				
	</main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
